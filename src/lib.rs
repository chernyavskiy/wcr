use clap::{App, Arg};
use std::convert::From;
use std::error::Error;
use std::fs::File;
use std::io::{self, BufRead, BufReader};

type MyResult<T> = Result<T, Box<dyn Error>>;

#[derive(Debug)]
pub struct Config {
    files: Vec<String>,
    lines: bool,
    words: bool,
    bytes: bool,
    chars: bool,
}

#[derive(Debug, PartialEq)]
pub struct FileInfo {
    num_lines: usize,
    num_words: usize,
    num_bytes: usize,
    num_chars: usize,
}

pub fn get_args() -> MyResult<Config> {
    let matches = App::new("wcr")
        .version("0.1.0")
        .author("Timofey Chernyavskiy <timofey.chernyavskiy@gmail.com>")
        .about("Rust wc")
        .arg(
            Arg::new("lines")
                .long("lines")
                .short('l')
                .help("Show line count")
                .takes_value(false),
        )
        .arg(
            Arg::new("words")
                .long("words")
                .short('w')
                .help("Show word count")
                .takes_value(false),
        )
        .arg(
            Arg::new("chars")
                .long("chars")
                .short('m')
                .help("Show character count")
                .takes_value(false),
        )
        .arg(
            Arg::new("bytes")
                .long("bytes")
                .short('c')
                .help("Show byte count")
                .conflicts_with("chars")
                .takes_value(false),
        )
        .arg(
            Arg::new("files")
                .allow_invalid_utf8(true)
                .value_name("FILE")
                .multiple_values(true)
                .help("Input file(s)")
                .default_value("-"),
        )
        .get_matches();

    let mut lines = matches.is_present("lines");
    let mut words = matches.is_present("words");
    let mut bytes = matches.is_present("bytes");
    let chars = matches.is_present("chars");
    if [lines, words, bytes, chars].iter().all(|v| !v) {
        lines = true;
        words = true;
        bytes = true;
    }

    Ok(Config {
        files: matches.values_of_lossy("files").unwrap(),
        lines,
        words,
        bytes,
        chars,
    })
}

fn print_file_info(file_info: &FileInfo, cfg: &Config, filename: &str) {
    println!(
        "{}{}{}{}",
        if cfg.lines {
            format!("{:>8}", file_info.num_lines)
        } else {
            String::new()
        },
        if cfg.words {
            format!("{:>8}", file_info.num_words)
        } else {
            String::new()
        },
        if cfg.bytes {
            format!("{:>8}", file_info.num_bytes)
        } else if cfg.chars {
            format!("{:>8}", file_info.num_chars)
        } else {
            String::new()
        },
        if filename != "-" {
            format!(" {}", filename)
        } else {
            String::new()
        }
    );
}

pub fn run(config: Config) -> MyResult<()> {
    let mut fis: Vec<FileInfo> = vec![];
    for filename in &config.files {
        match open(filename) {
            Err(err) => eprintln!("{}: {}", filename, err),
            Ok(file) => {
                let fi: FileInfo = count(file)?;
                print_file_info(&fi, &config, filename);
                fis.push(fi);
            }
        }
    }
    if config.files.len() > 1 {
        let total = fis.iter().fold(
            FileInfo {
                num_lines: 0,
                num_words: 0,
                num_bytes: 0,
                num_chars: 0,
            },
            |acc, fi| FileInfo {
                num_lines: acc.num_lines + fi.num_lines,
                num_words: acc.num_words + fi.num_words,
                num_bytes: acc.num_bytes + fi.num_bytes,
                num_chars: acc.num_chars + fi.num_chars,
            },
        );
        print_file_info(&total, &config, "total");
    }
    Ok(())
}

fn open(filename: &str) -> MyResult<Box<dyn BufRead>> {
    match filename {
        "-" => Ok(Box::new(BufReader::new(io::stdin()))),
        _ => Ok(Box::new(BufReader::new(File::open(filename)?))),
    }
}

pub fn count(mut file: impl BufRead) -> MyResult<FileInfo> {
    let mut num_lines = 0;
    let mut num_words = 0;
    let mut num_chars = 0;
    let mut num_bytes = 0;
    let mut line = String::new();

    loop {
        match file.read_line(&mut line) {
            Ok(0) => break,
            Ok(bytes_num) => {
                num_bytes += bytes_num;
                num_lines += 1;
                num_words += line.split_whitespace().count();
                num_chars += line.chars().count();
                line.clear();
            }
            Err(err) => return Err(From::from(err)),
        }
    }

    Ok(FileInfo {
        num_lines,
        num_words,
        num_bytes,
        num_chars,
    })
}

#[cfg(test)]
mod tests {
    use super::{count, FileInfo};
    use std::io::Cursor;

    #[test]
    fn test_count() {
        let text = "I don't want the world. I just want your half.\r\n";
        let info = count(Cursor::new(text));
        assert!(info.is_ok());
        let expected = FileInfo {
            num_lines: 1,
            num_words: 10,
            num_chars: 48,
            num_bytes: 48,
        };
        assert_eq!(info.unwrap(), expected);
    }
}
